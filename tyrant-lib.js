function getStartDate(args)
{
    var startDate = new Date();
    if (args.length > 2)
    {
        var month = args[2];
        return new Date(startDate.getFullYear(), args[2]);
    }
    return startDate;
}
exports.getStartDate = getStartDate;

function fisherYatesShuffle(o)
{
    var N = o.length;
    for (var index = 0; index < N; index++)
    {
        var swapIndex = index + Math.floor(Math.random() * (N-index));
        var temp = o[swapIndex];
        o[swapIndex] = o[index];
        o[index] = temp;
    } 
}
exports.fisherYatesShuffle = fisherYatesShuffle;


