var tyrantLib = require('../tyrant-lib.js');

var hashMap = new Object();
var tyrants = initializeTyrants();

for (var i=0; i < 10000; i++)
{
    tyrantLib.fisherYatesShuffle(tyrants);
    if(!hashMap.hasOwnProperty(tyrants))
    {
        hashMap[tyrants] = 1;
    } else {
        hashMap[tyrants]++;
    }

    // re-initialize - for fairness.
    tyrants = initializeTyrants();
}
//console.log(hashMap);
var mean = getMean(hashMap);
console.log('mean : ' + mean);
var standardDeviation = getStandardDeviation(hashMap, mean);
console.log('sDev : ' + standardDeviation);

function initializeTyrants()
{
    return ['a', 'b', 'c', 'd'];
}

function getMean(hashMap)
{
    var mean = 0.0;
    var count = 0;
    var total = 0;
    for (key in hashMap)
        if (hashMap.hasOwnProperty(key)) 
        {
            count++;
            total += hashMap[key];
        }
    console.log('count : ' + count);
    console.log('total : ' + total);
    mean = total/count;
    return mean;
}

function getStandardDeviation(hashMap, mean)
{
    var varience = 0.0;
    for (key in hashMap)
    {
        if (hashMap.hasOwnProperty(key))
        {
            var vItem = hashMap[key] - mean;
            varience += (vItem*vItem);
        }
    }
    
    var standardDeviation = Math.sqrt(varience);
    return standardDeviation;
}
