var tyrantOrderModule = require('../tyrant-order.js');

describe('create tyrants', function() {
        it('should create a single tyrant for month 0', function() {
            var tyrantNames = ['test'];
            var startDate = new Date(2014, 0);
            var tyrants = tyrantOrderModule.createTyrants(tyrantNames, startDate);
            expect(tyrants.length).toBe(tyrantNames.length);
            expect(tyrants[0].tyrantName).toBe(tyrantNames[0]);
            expect(tyrants[0].monthToRule).toBe('Jan');
            });

        it('should create three tyrants starting in March', function() {
            var tyrantNames = ['Alice', 'Bob', 'Carol'];
            var startDate = new Date(2014, 2);
            var tyrants = tyrantOrderModule.createTyrants(tyrantNames, startDate);
            expect(tyrants.length).toBe(tyrantNames.length);
            expect(tyrants[0].tyrantName).toBe(tyrantNames[0]);
            expect(tyrants[1].tyrantName).toBe(tyrantNames[1]);
            expect(tyrants[2].tyrantName).toBe(tyrantNames[2]);
            expect(tyrants[0].monthToRule).toBe('Mar');
            expect(tyrants[1].monthToRule).toBe('Apr');
            expect(tyrants[2].monthToRule).toBe('May');
            });
        
        it('should create three tyrants starting in December', function() {
            var tyrantNames = ['Alice', 'Bob', 'Carol'];
            var startDate = new Date(2014, 11);
            var tyrants = tyrantOrderModule.createTyrants(tyrantNames, startDate);
            expect(tyrants.length).toBe(tyrantNames.length);
            expect(tyrants[0].tyrantName).toBe(tyrantNames[0]);
            expect(tyrants[1].tyrantName).toBe(tyrantNames[1]);
            expect(tyrants[2].tyrantName).toBe(tyrantNames[2]);
            expect(tyrants[0].monthToRule).toBe('Dec');
            expect(tyrants[1].monthToRule).toBe('Jan');
            expect(tyrants[2].monthToRule).toBe('Feb');
            });
});
