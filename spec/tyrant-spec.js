var tyrantModule = require("../tyrant.js");

describe("toString", function() {
        it("should return the string representation of the tyrant object", function() {
            var t = new tyrantModule.tyrant("name", "month");
            var result = t.toString();
            expect(result).toBe("name holds dominion over month");
            });
        });
        
