var tyrantLib = require('../tyrant-lib.js');

describe('Read the zero-indexed date from the commandline arguments array', function() {
        it("should return a date object set to the current month for no commandline arguments", function() {
            var processArgs = [ 'node', 'fileName' ];
            var result = tyrantLib.getStartDate(processArgs);
            var expected = new Date();
            expect(result.getMonth()).toBe(expected.getMonth());
            });

        it("should return a date object set to the specified month for a single commandline argument", function() {
            var processArgs = [ 'node', 'fileName', '2' ];
            var result = tyrantLib.getStartDate(processArgs);
            var expected = new Date();
            expect(result.getMonth()).toBe(parseInt(processArgs[2]));
            });

        it("should treat '12' '0' because months are 0 indexed", function() {
            var processArgs = [ 'node', 'fileName', '12' ];
            var result = tyrantLib.getStartDate(processArgs);
            var expected = new Date();
            expect(result.getMonth()).toBe(0);
            });
        });

describe('shuffle test', function() {
        it('should keep all the existing items in the array', function() {
            var inputArray = ['a','b','c','d','e'];
            var expectedLength = inputArray.length;
            tyrantLib.fisherYatesShuffle(inputArray);
            expect(inputArray.length).toBe(expectedLength);
            expect(inputArray.indexOf('a')).not.toBe(-1);
            expect(inputArray.indexOf('b')).not.toBe(-1);
            expect(inputArray.indexOf('c')).not.toBe(-1);
            expect(inputArray.indexOf('d')).not.toBe(-1);
            expect(inputArray.indexOf('e')).not.toBe(-1);
            });
        });


