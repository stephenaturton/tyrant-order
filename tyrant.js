function tyrant(tyrantName, monthToRule)
{
    var that = {};
    that.tyrantName = tyrantName;
    that.monthToRule = monthToRule;

    that.toString = function() {
        return that.tyrantName + ' holds dominion over ' + that.monthToRule;
    };
    return that;

exports.tyrant = tyrant;
