var tyrantModule = require("./tyrant.js");
var tyrantLib = require("./tyrant-lib.js");

var tyrantNames = ['Dave', 'Edwin', 'Julian', 'Kev', 'Phil', 'Ste', 'Steve'];
tyrantLib.fisherYatesShuffle(tyrantNames);
var tyrants = createTyrants(tyrantNames, tyrantLib.getStartDate(process.argv));
tyrants.forEach(function(tyrant) {
            console.log(tyrant.toString());
        });

function createTyrants(tyrantNames, startDate)
{
    var tyrants = [];
    var date = startDate;
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    for(var i = 0; i < tyrantNames.length; i++)
    {
        tyrants[i] = new tyrantModule.tyrant(tyrantNames[i], monthNames[date.getMonth()]);
        date = new Date(date.getFullYear(), date.getMonth()+1);
    }
    return tyrants;
}
exports.createTyrants = createTyrants;
